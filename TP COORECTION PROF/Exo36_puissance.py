"""Programme calculant x puissance y, x rÃ©el et y entier"""

def calculerPuissance(x,y) :
    """Fonction rÃ©cursive renvoyant x puissance y, avec x rÃ©el et y entier quelconque (positif ou nÃ©gatif :
    . formules de rÃ©cursivitÃ© : 
            . si y>=0 alors x^y = x * x^(y-1)
            . si y<0 alors x^y = (1/x)^(-y), et on peut appliquer la formule prÃ©cÃ©dente sauf si x==0, auquel cas on renvoie 0
    . test d'arrÃªt : si y==0, alors x^0==1 (on peut aussi s'arrÃªter sur y==1)

    ParamÃ¨tres attendus : x rÃ©el, y entier
    Valeur de retour : rÃ©el
    """

    """tests d'arrÃªt"""
    if x==0 or x==1 : #Ã  x==0 qu'on doit tester Ã  cause de la possibilitÃ© du y<0, on peut ajouter x==1 car dans ces 2 cas on a la rÃ©ponse quelque soit y
        return x
    if y==0 :
        return 1
    """rÃ©cursivitÃ©"""
    if y>0 :
        return x*calculerPuissance(x,y-1)
    #quand on arrive ici x!=0 et x!=1 et y<0
    return calculerPuissance(1/x, -y) #on applique la formule de rÃ©cursivitÃ©

"""Programme principal"""
"""Saisie des nombres x et y"""
x = float(input("Entrez un rÃ©el : "))
y = int(input("Entrez un entier : "))

"""Affichage de x puissance y"""
print("("+str(x)+")^("+str(y)+") = ",calculerPuissance(x,y))
