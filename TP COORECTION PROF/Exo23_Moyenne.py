def moy4():
    """Saisie de 4 entiers et calcul de la moyenne
    Argument : aucun
    Valeur de retour : réel, moyenne
    """

    """Comme dans l'exercice 21, on va ajouter les valeurs entières après chaque saisie
    On va donc utiliser une boucle qui permet de faire 4 fois : 
    saisir un entier et l'ajouter dans la somme
    """
    somme = 0 #initialisation de la somme
    for i in range(4) : #va faire 4 fois les instructions pour i=0, puis i=1, i=2 et i=3
        valeur = int(input("Entier ? "))
        somme = somme+valeur
    return somme/4.0

"""On affiche la moyenne de ces 4 entiers"""
print("La moyenne de ces 4 entiers est : "+str(moy4()))
