"""Programme calculant les termes de la suite de Fibonacci"""

def calculerFibonacci(n) :
    """Fonction rÃ©cursive renvoyant le niÃ¨me terme de la suite de Fibonacci, avec  entier positif strictement :
    . tests d'arrÃªt : si n==1 ou n==2, alors Fibo(n) =   1
    . formule de rÃ©cursivitÃ© : fibo(n) = fibo(n-1) + fibo(n-2)

    ParamÃ¨tres attendus : n, entier
    Valeur de retour : entier
    """

    """test d'arrÃªt"""
    if n==1 or n==2 : 
        return 1
    
    """rÃ©cursivitÃ©"""
    return calculerFibonacci(n-1) + calculerFibonacci(n-2)

"""Programme principal"""
"""Saisie du terme N"""
N = int(input("Entrez un entier positif non nul : "))

"""Affichage des valeurs de Fibo(1) Ã  Fibo(N)"""
print("La suite de Fibonacci("+str(N)+") : ",end="")
for terme in range(1,N+1) :
    print(calculerFibonacci(terme),end=" ")
print()
