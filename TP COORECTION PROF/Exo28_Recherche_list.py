"""Manipulation des Ã©lÃ©ments d'une liste"""

"""inclusion des librairies"""
from random import * #pour la fonction randrange()

def createList(N):
    """CrÃ©ation de la liste avec N valeurs alÃ©atoires entre 0 et N-1

    Argument : entier
    Valeur de retour : liste
    """
    L = []
    for elt in range(N) : #elt va prendre les valeurs 0, puis 1, puis 2, ..., N-1
        L.append(randrange(N))
    return L

def afficheNbCol(L, nbCol):
    """On affiche la liste nbCol Ã©lÃ©ments par ligne :
    . on va donc afficher les nbCol premiers Ã©lÃ©ments d'indice allant de 0 Ã  nbCol-1 => for indice in range(0,nbCol,1)
    . puis les nbCol suivants de nbCol Ã  2*nbCol-1 => for indice in range(nbCol,2*nbCol,1)
    . puis les nbCol suivants de 2*nbCol Ã  3*nbCol-1 => for indice in range(2*nbCol,3*nbCol,1)
    .... ainsi de suite on va pouvoir afficher len(L)//nbCol lignes complÃ¨tes et une derniÃ¨re ligne avec le reste  
    . donc on va afficher (len(L)//nbCol) fois : for indice in range(a*nbCol,b*nbCol,1) avec b=a+1 et a qui prend les valeurs 0, 1, 2 jusqu'Ã  (len(L)//nbCol) 
    . puis la derniÃ¨re ligne incomplÃ¨te avec les Ã©lÃ©ments de (len(L)//nbCol)*nbCol Ã  len(L) => for indice in range((len(L)//nbCol)*nbCol, len(L), 1) 

    Arguments : liste, entier
    Valeur de retour : aucune
    """
    nbLignes = len(L)//nbCol
    #on affiche les nbLignes lignes pleines
    for ligne in range(nbLignes) : #ligne va prendre 0, 10, 20, 30, 40
        for indice in range(ligne*nbCol, (ligne+1)*nbCol, 1) :
            print(L[indice],end=" ")
        print() #pour passer Ã  la ligne
    #puis on affiche la derniÃ¨re ligne incomplÃ¨te
    for indice in range(nbLignes*nbCol, len(L), 1) :
        print(L[indice],end=" ")
    print()
    
def valPlusPetit(L):
    """Recherche de la plus petite valeur de la liste L. 
    . il faut une premiÃ¨re valeur de comparaison => on prendra le premier Ã©lÃ©ment (L[0])
    . ensuite on va parcourir la liste Ã  partir du 2e Ã©lement (L[1]), 
    . et comparer chaque Ã©lÃ©ment (L[i], avec $i \geq 1$) avec la valeur la plus petite trouvÃ©e (= sauvegardÃ©e)

    Argument : liste
    Valeur de retour : entier
    """
    PP = 0 #le plus petit trouvÃ© jusque lÃ  est le 1er Ã©lÃ©ment d'indice 0
    for courant in range(1, len(L)) : #courant prend des valeurs de 1 Ã  len(L)-1
        if L[courant] < PP : #si l'Ã©lÃ©ment courant a une valeur plus petite que celle dÃ©jÃ  trouvÃ©e
            PP = L[courant] #on sauve cette nouvelle valeur plus petite
    return PP

def indicePlusPetit_AB(L,a,b):
    """Recherche de l'indice de l'Ã©lÃ©ment de plus petite valeur parmi les Ã©lÃ©ments de la sous liste (L[a], L[a+1], ..., L[b-1]). 
    . la recherche sera similaire Ã  la mÃ©thode vue dans la fonction prÃ©cÃ©dente, sauf que 
    c'est l'indice dans la liste de la valeur la plus petite et non la valeur

    Arguments : liste, 2 entiers
    Valeur de retour : entier
    """
    indicePP = a #le plus petit trouvÃ© jusque lÃ  est le 1er Ã©lÃ©ment de la sous-liste, soit L[a]
    for courant in range(a+1, b) : #indice prend des valeurs de a+1 Ã  b-1
        if L[courant] < L[indicePP] : #si l'Ã©lÃ©ment courant a une valeur plus petite que celle dÃ©jÃ  trouvÃ©e
            indicePP = courant #on sauve l'indice de cette nouvelle valeur plus petite
    return indicePP
            

def triMin(L):
    """Tri minimum (ou par sÃ©lection)
    . trouver le plus petit Ã©lement de la liste et le mettre dans la 1Ã¨re case revient Ã  Ã©craser la valeur qui Ã©tait dans la 1Ã¨re case, donc il faut qu'on puisse se souvenir de cette valeur et la revisiter dans la prochaine recherche : une solution est de la mettre dans la case qui contenait la plus petite valeur. Donc on va :
    1. rechercher l'indice de la plus petite valeur de toute la liste
    2. Ã©changer le contenu de la 1Ã¨re case avec le contenu de la case dont on a sauvegardÃ© la position
    . ensuite on recommence 1. et 2. sur la sous-liste privÃ©e du 1er Ã©lÃ©ment, puis sur la sous-liste privÃ©e des 2 premiers Ã©lÃ©ments ...

    Argument : liste
    Valeur de retour : liste
    """
    for elementInitial in range(len(L)) : #le 1er Ã©lÃ©ment sera 0, puis 1, puis 2, 
        positionPP = indicePlusPetit_AB(L,elementInitial,len(L)) #on recherche la position du plus petit Ã©lÃ©ment dans la sous-liste de L[elementInitial] Ã  L[len(L)-1]
        if positionPP != elementInitial : #l'Ã©lÃ©ment de plus petite valeur n'est pas le 1er de la sous-liste
                                      #on Ã©change alors cet Ã©lÃ©ment et le 1er
            tmp = L[elementInitial] #valeur du 1er Ã©lÃ©ment de la sous-liste parcourue
            L[elementInitial] = L[positionPP] #on met la plus petite valeur dans le 1er Ã©lÃ©ment de la sous-liste
            L[positionPP] = tmp  #on met la valeur du 1er Ã©lÃ©ment de la sous-liste dans la case qui contenait la plus petite valeur
    return L


"""Programme principal"""
taille = int(input("Quelle taille pour votre liste ? "))
L = createList(taille)
print("Liste aleatoire : ")
nbCol = int(input("Combien d'elements par ligne voulez-vous afficher ? "))
afficheNbCol(L, nbCol)
print("La plus petite valeur de la liste est : "+str(valPlusPetit(L)))
print("Liste triÃ©e : ")
Ltriee = triMin(L) #du coup on a 2 listes diffÃ©rentes L non triÃ©e et Ltriee
afficheNbCol(L, nbCol-2) #pour vÃ©rifier que l'affichage fonctionne avec diffÃ©rentes valeurs
