from random import *

def valAlea(N) :
  return -1 + randrange(N+2)

def initListe(N) :
  L = []
  val = valAlea(N)
  #print("val = ",val,end=" ") #debug

  while val != -1 :
    val = valAlea(N)
    L.append(val)
    
    #print("val = ",val,end=" ") #debug
  return L

def occurrenceListe(L, val) :
  nbOcc = 0
  for elt in range(len(L)) : #elt 0, 1, 2, ..., len(L)-1
    if L[elt] == val :
      nbOcc = nbOcc + 1
  return nbOcc

"""Programme principal"""
max = int(input("N ? "))
#print("Une valeur alÃ©atoire est : "+str(valAlea(max)))
Liste = initListe(max)
print(Liste)

valeur = int(input("Entrez une valeur Ã  rechercher : "))
print("Il y a dans la liste "+str(occurrenceListe(Liste, valeur))+" fois la valeur "+str(valeur))
