"""Programme donnant le carré d'un nombre"""

def calculerCarre(x) :
    """Fonction renvoyant le carré de x

    Paramètre attendu : x, réel
    Valeur de retour : réel, carré de x
    """
    return x*x


"""Programme principal"""
"""Saisie d'un nombre"""
nombre = float(input("Entrez un nombre : "))

"""Affichage de son carré"""
"""la fonction "calculerCarre renvoie un nombre qu'on peut afficher directement
On pourrait aussi sauvegarder le résultat dans une variable et afficher le contenu de cette variable ensuite (mais c'est inutile si on n'a pas besoin de réutiliser le carré de ce nombre dans un calcul :
nbCarre = calculerCarre(nombre)
print("carre("+str(nombre)+") = ",nbCarre)
"""
print("carre("+str(nombre)+") = ",calculerCarre(nombre))
