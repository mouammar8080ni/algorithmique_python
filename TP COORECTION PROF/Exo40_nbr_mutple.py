"""Programme permettant de savoir si 2 nombres sont multiples"""

def echanger(a,b) :
    return b,a

def estMultiple(x,y) :
    """Fonction rÃ©cursive renvoyant 1 si x et y sont multiples et 0 sinon :
    . formule de rÃ©cursivitÃ© : estMultiple(x,y) = estMultiple(x-y,y) x doit Ãªtre supÃ©rieur Ã  y
    . tests d'arrÃªt : 
              . si x==0, alors x et y sont multiples on renvoie 1
              . si x<0, alors x et y ne sont pas multiples et on renvoie 0

    ParamÃ¨tres attendus : x et y, entiers
    Valeur de retour : 0 (faux) ou 1 (vrai)
    """

    """test d'arrÃªt"""
    if x==0  : 
        return 1
    if x<0 :
        return 0

    """rÃ©cursivitÃ©"""
    return estMultiple(x-y,y) #on applique la formule de rÃ©cursivitÃ©


"""Programme principal"""
"""Saisie des nombres a et b"""
a = int(input("Entrez un entier : "))
b = int(input("Entrez un entier : "))
if a<b : #il faut qu'on teste en ayant a>b ... on les Ã©change !
    a,b = echanger(a,b)
"""Affichage de x est multiple de y"""
if estMultiple(a,b) : #si vaut 1 (alors la condition est vue vrai donc va exÃ©cuter le "print("est multiple")
    print(str(a)+" et "+str(b)+" sont multiples")
else :
    print(str(a)+" et "+str(b)+" ne sont pas multiples")
