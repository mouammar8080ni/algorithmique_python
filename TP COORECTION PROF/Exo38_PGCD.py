"""Programme calculant le PGCD et PPCM de 2 nombres"""

def calculerPgcd(x,y) :
    """Fonction rÃ©cursive renvoyant le PGCD de x et de y, x et y Ã©tant des entiers positifs :
    . formule de rÃ©cursivitÃ© : PGCD(x,y) = PGCD(y,x%y) 
    . test d'arrÃªt : si y==0, alors PGCD(x,y)==x

    ParamÃ¨tres attendus : x et y, entiers
    Valeur de retour : entier
    """

    """test d'arrÃªt"""
    if y==0  : 
        return x


    """rÃ©cursivitÃ©"""
    return calculerPgcd(y, x%y) #on applique la formule de rÃ©cursivitÃ©

def calculerPpcm(x,y) :
    """Fonction non rÃ©cursive renvoyant le PPCM de x et de y, x et y Ã©tant des entiers positifs :
    . formule  : PPCM (x,y) = (x*y) / PGCD(x,y), on est sÃ»r que la division est entiÃ¨re et que PGCD!=0

    ParamÃ¨tres attendus : x et y, entiers
    Valeur de retour : entier
    """

    return (x*y) / calculerPgcd(x,y)

    """Programme principal"""
"""Saisie des nombres x et y"""
a = int(input("Entrez un entier : "))
b = int(input("Entrez un entier : "))

"""Affichage de x puissance y"""
print("PGCD("+str(a)+","+str(b)+") = ",calculerPgcd(a,b))
print("PPCM("+str(a)+","+str(b)+") = ",calculerPpcm(a,b))
