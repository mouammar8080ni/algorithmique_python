def plusGrand_10():
    """Saisie de 10 entiers et affichage du plus grand"""
    """Comme lorsqu'on calcule une somme, on ajoute les éléments au fur et à mesure 
    qu'on les lit.
    Chaque fois qu'on lit un entier on le compare au plus grand trouvé jusque là :
    . s'il est plus grand ça devient le nouveau plus grand
    . sinon, on ne fait rien
    """

    """On va donc saisir 10 fois un entier avec l'exception qui est de sauvegarder 
    le premier entier saisi comme le plus grand jusque là.
    Puis seulement après on va pouvoir faire 10-1 fois : 
    . lire une valeur, 
    . si elle est plus grande que le plus grand jusque là, on remplace le plus grand par cette valeur 

    Argument : aucun
    Valeur de retour : entier,plus grand
    """
    plusGrand = int(input("Entier 1 ? ")) #on sauvegarde tout de suite la 1ère valeur comme la plus grande

    for num in range(2, 11) : #on utilise "num" pour mettre le numéro des entiers à lire
        valeur = int(input("Entier "+str(num)+" ? ")) #on est obligé de constituer une seule chaîne de caractères 
        if valeur > plusGrand :
            plusGrand = valeur
    return plusGrand

def plusGrand_N(N):
    """Si maintenant on saisit N entiers, 
    on va juste modifier la valeur max des numéros (et donc du nombre d'entiers)
    
    Argument : aucun
    Valeur de retour : entier,plus grand
    """
    plusGrand = int(input("Entier 1 ? ")) #on sauvegarde tout de suite la 1ère valeur comme la plus grande
    for num in range(2, N+1) : #on utilise "num" pour mettre le numéro des entiers à lire, on en a nbEntiers-1 à lire en partant de 2
        valeur = int(input("Entier "+str(num)+" ? "))
        if valeur > plusGrand :
            plusGrand = valeur
    return plusGrand

"""Programme principal"""
print("Plus grand parmi 10 entiers")
pg = plusGrand_10()
print("Le plus grand entier saisi est ",pg)

nbEntiers = int(input("Nombre d'entiers ? "))
print("Plus grand parmi ",nbEntiers, " entiers")
pg = plusGrand_N(nbEntiers)
print("Le plus grand entier saisi est ",pg)

