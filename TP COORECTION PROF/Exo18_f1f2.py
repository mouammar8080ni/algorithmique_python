"""Programme calculant g(x,y) à partir de 2 fonctions f1(x) et f2(x)"""
"""Ce programme a comme utilité de voir une fonction en appeler une autre"""

def f1(x) :
    """Fonction renvoyant 3x*x+x+1 si x est pair et x+1 sinon

    Paramètre attendu : x, entier
    Valeur de retour : entier,  
    """
    if not(x%2) : #not(x%2) est équivalent à x%2==0, donc on regarde si x est pair
        return 3*x*x+x+1 #ou 3*x**2+x+1
    return x+1 #toujours pareil si x est pair alors la fonction n'atteint pas cette instruction (elle est arrêtée avec le renvoi de la valeur3*x*x+x+1)
               #cette instruction est donc atteinte dans le cas où x n'est pas pair

def f2(x) :
    """Fonction renvoyant 3*x*x si x est positif ou -3*x*x s'il est négatif

    Paramètre attendu : x, entier
    Valeur de retour : entier,  f2(x)
    """
    if x >=0 :
        return 3*x**2
    return -3*x*x

def g(x,y) :
   """Fonction renvoyant f1(x) + f2(y)

    Paramètres attendus : x et y, entiers
    Valeur de retour : entier,  g(x,y)
    """ 
   return f1(x)+f2(y)

def gSansf1f2(x,y) :
    """Fonction renvoyant f1(x) + f2(y)
    
    Paramètres attendus : x et y, entiers
    Valeur de retour : entier,  g(x,y)
    """
    if y >=0 :
        if not(x%2) :
            return 3*x*x+x+1+3*y**2
        return x+1+3*y**2
    else :
        if not(x%2) : 
            return 3*x*x+x+1-3*y**2
        return x+1-3*y**2
   
"""Autre maniere de faire"""
def gSansf1f2Bis(x,y) :
    """Fonction renvoyant f1(x) + f2(y)
       
    Paramètres attendus : x et y, entiers
    Valeur de retour : entier,  g(x,y)
    """
    res = 0
    if y >=0 :
        res = res+3*y**2
    else :
        res = res-3*y**2
        
    if not(x%2) :
        res = res+3*x*x+x+1
    else :
        res = res+x+1

    return res

"""Programme principal"""
"""Saisie de l'entier dont on veut calculer l'image par f2"""
x = int(input("Entrez un entier : "))
y = int(input("Entrez un entier : "))

"""Affichage des valeurs de f1(x) et f2(y) et g(x,y)"""
print("f1("+str(x)+") = ",f1(x))
print("f2("+str(y)+") = ",f2(y))
print("g(",x,","+str(y)+") = ",g(x,y))
print("g(",x,","+str(y)+") = ",gSansf1f2(x,y))
print("g(",x,","+str(y)+") = ",gSansf1f2Bis(x,y))

