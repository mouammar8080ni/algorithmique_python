"""Recherche de l'indice d'un Ã©lÃ©ment dans une liste"""
from random import * #pour les valeurs alÃ©atoires de la liste

def initList(n) :
    """Fonction retournant une liste de n listes de n entiers remplie alÃ©atoirement = matrice carrÃ©e nxn
    
    ParamÃ¨tre attendu : n : entier (taille de la matrice carrÃ©e)
    Valeur de retour : liste
    """
    L = [] 
    for elt in range(n) :
        sousL = []
        for i in range(n):
            sousL.append(randrange(n))
        L.append(sousL)
        
    return L

def afficheList(L) :
    """Fonction affichant une liste de listes d'entiers (matrice) ligne par ligne
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Pas de valeur de retour :
    """
    n=len(L) #taille de la matrice carrÃ©e n listes de n Ã©lÃ©ments
    for ligne in range(n) :
        for colonne in range(n):
            print(L[ligne][colonne],end=" ")
        print()
        
def sommeDiag(L) :
    """Fonction retournant la position de la valeur x dans la liste L

    ParamÃ¨tres attendus : L : liste d'entiers, x : entier
    Valeur de retour : entier : position de x dans L ou -1 si x non trouvÃ©
    """
    #on parcourt la liste et dÃ¨s qu'on a trouvÃ© x on s'arrÃªte et on retourne la postion
    somme = 0
    for i in range(len(L)) : 
        somme = somme + L[i][i]
    
    return somme #si on sort du "for" c'est qu'on n'a pas trouvÃ© x dans L, on n'a pas pu exÃ©cuter "return position"

            
"""Programme principal"""
"""Saisie de la liste de dÃ©part, on demande le nombre d'Ã©lÃ©ments"""
N = int(input("Entrez le nombre d'Ã©lÃ©ments de la liste : "))
matrice = initList(N)
print(matrice) #affiche la liste L sous forme de liste
afficheList(matrice) #affiche la liste sous forme de matrice
"""Affichage de la position si la recherche a abouti"""
print("somme des diagonales = "+str(sommeDiag(matrice)))
