"""Recherche de voyelles dans une chaÃ®ne de caractÃ¨res"""


def compterVoyelles(L) :
    """Fonction renvoyant le nombre de voyelles dans la chaÃ®ne de caractÃ¨res L:
    . une chaÃ®ne de caractÃ¨res peut Ãªtre vue comme une liste de caractÃ¨res
    . une voyelle est une lettre Ã©gale 'a', 'e', 'i', 'o', 'u', 'y'
    . on va donc parcourir la liste et comparer chaque caractÃ¨re aux valeurs listÃ©es ci-dessus
 
   ParamÃ¨tre attendu : L, liste de caractÃ¨res
    Valeur de retour : entier (nombre de voyelles)
    """

    nbVoyelles = 0 #contient le nombre de voyelles
    for i in range(len(L)) : #on parcourt la liste, on regarde chaque Ã©lÃ©ment
        if L[i]=='a' or L[i]=='e' or L[i]=='i' or L[i]=='o' or L[i]=='u' or L[i]=='y':
            nbVoyelles = nbVoyelles + 1
    """Quand on sort de la boucle, on a vu tous les caractÃ¨res, on peut renvoyer le nombre de voyelles trouvÃ©"""        
    return nbVoyelles


"""Programme principal : on teste si une chaÃ®ne de caractÃ¨res saisie est un palindrome"""
L = input("Entrez une chaÃ®ne de caractÃ¨res (mot ou phrase avec des mots sÃ©parÃ©s par des espaces) : ")

print("Dans '"+L+"', il y a "+str(compterVoyelles(L))+" voyelles")
    
