"""Palindrome"""


def palindrome(L,first,last) :
    """Fonction renvoyant 1 si la chaÃ®ne de caractÃ¨res L est un palindrome
    et 0 sinon
    ParamÃ¨tre attendu :
      . L, liste de caractÃ¨res
      . first, position du premier caractÃ¨re de la liste L Ã  comparer
      . last, position du dernier caractÃ¨re de la liste L Ã  comparer

    Valeur de retour : 1 si L est un palindrome, 0 sinon
    """
    """positions des caractÃ¨res Ã  comparer"""
    if (first < last) : #il reste des caractÃ¨res Ã  comparer
        """on ne compte pas les espaces"""
        if L[first]==" " : #on recommence avec le caractÃ¨re suivant
            return palindrome(L,first+1,last)
        if L[last]==" ": #on recommence avec le caractÃ¨re prÃ©cÃ©dent
            return palindrome(L,first,last-1)
        """si les 2 caractÃ¨res sont Ã©gaux, on recommence avec le suivant et le prÃ©cÃ©dent
        sinon on retourne 0"""
        if L[first]==L[last] :
            return palindrome(L,first+1,last-1)
        else : 
            return 0
    """quand on est lÃ , c'est que first>=last, on a fini sans sortir par 0, L est un palindrome"""
    return 1


"""Programme principal : on teste si une chaÃ®ne de caractÃ¨res saisie est un palindrome"""
L = input("Entrez une chaÃ®ne de caractÃ¨res (mot ou phrase avec des mots sÃ©parÃ©s par des espaces) : ")

"""on appelle la fonction palindrome sur les Ã©lÃ©ments de la chaÃ®ne avec:
. le premier praramÃ¨tre est la liste
. le 2e paramÃ¨tre correspond Ã  la position du premier caractÃ¨re Ã  comparer : 0
. le 3e paramÃ¨tre correspond Ã  la position du dernier caractÃ¨re Ã  comparer : len(L)-1
"""
if palindrome(L,0,len(L)-1) :
    print("'"+L+"' est un palindrome")
else :
    print("'"+L+"' n'est pas un palindrome")
    
