def estPair(n) :
	"""Fonction renvoyant Vrai si n est pair et Faux sinon
	
	Argument : entier, n
	Valeur de retour : booléen
	"""
	return (n%2==0)
	
"""Programme principal : appel de la fonction"""
nat = int(input("Veuillez saisir un entier svp : "))
if estPair(nat) :
	print(nat," est pair")
else :
	print(nat," est impair")	

