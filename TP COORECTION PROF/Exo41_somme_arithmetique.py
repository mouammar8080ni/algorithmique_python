"""Programme permettant de calucler la somme des N premiers entiers"""

def calculerSommeN(n) :
    """Fonction rÃ©cursive renvoyant la somme des n premiers entiers naturels (n>=0):
    . formule de rÃ©cursivitÃ© : somme(n) = n + somme(n-1)
    . test d'arrÃªt : 
              . si n==0, alors somme(0) == 0        

    ParamÃ¨tres attendus : n, entier
    Valeur de retour : entier, somme
    """

    """test d'arrÃªt"""
    if n==0  : 
        return 0

    """rÃ©cursivitÃ©"""
    return n + calculerSommeN(n-1) #on applique la formule de rÃ©cursivitÃ©


"""Programme principal"""
"""Saisie de N"""
N = int(input("Entrez un entier positif ou nul : "))
while N<0 :
    N = int(input("Entrez un entier positif ou nul : "))


"""Affichage de la somme des N premiers entiers naturels"""
print("La somme des "+str(N)+" premiers entiers est Ã©gale Ã  : "+str(calculerSommeN(N)))
