"""Recherche de l'indice d'un Ã©lÃ©ment dans une liste"""
from random import * #pour les valeurs alÃ©atoires de la liste

def position(L, x) :
    """Fonction retournant la position de la valeur x dans la liste L

    ParamÃ¨tres attendus : L : liste d'entiers, x : entier
    Valeur de retour : entier : position de x dans L ou -1 si x non trouvÃ©
    """
    #on parcourt la liste et dÃ¨s qu'on a trouvÃ© x on s'arrÃªte et on retourne la postion
    for position in range(len(L)) : 
        if L[position] == x : #si on trouve x, on renvoie sa position dans L
            return position
    
    return -1 #si on sort du "for" c'est qu'on n'a pas trouvÃ© x dans L, on n'a pas pu exÃ©cuter "return position"

def initList(n) :
    """Fonction retournant une liste de n entiers remplie alÃ©atoirement
    
    ParamÃ¨tre attendu : n : entier (nombre d'Ã©lÃ©ments de la liste)
    Valeur de retour : liste
    """
    L = [] 
    for elt in range(n) :
        L.append(randrange(n))
        
    return L
    
"""Programme principal"""
"""Saisie de la liste de dÃ©part, on demande le nombre d'Ã©lÃ©ments"""
N = int(input("Entrez le nombre d'Ã©lÃ©ments de la liste : "))
L = initList(N)
print(L)
nombre = int(input("Entrez le terme recherchÃ© : "))

"""Affichage de la position si la recherche a abouti"""
pos = position(L, nombre)
if (pos == -1) :
    print(str(nombre)+" n'existe pas dans la liste : ",end="")
    print(L)
else :
    print(str(nombre)+" se trouve Ã  la position "+str(pos)+" dans la liste : ",end="")
    print(L)
