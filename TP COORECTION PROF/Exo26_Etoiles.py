def etoiles(N):
    """Affichage d'une colonne de N étoiles
    Argument : entier
    Valeur de retour : aucune
    """
    print("Colonne de ",N," étoiles")
    for ligne in range(N) :
        print("*")
    print()

def triangle(N):
    """Affichage d'un triangle isocèle de N étoiles de côté
    Un triangle isocèle est un triangle qui a 2 côtés égaux :
    on va donc faire un triangle d'étoiles qui aura une hauteur de N lignes 
    dont le sommet aura une étoile au milieu de la 1ère ligne, => N-1 blancs + 1 étoile
    puis sur la 2e ligne on aura 3 étoiles donc celle du milieu sera en dessous de celle de la 1ère ligne => N-2 blancs + 3 étoiles
    ...
    la ligne i aura N-i blancs + 2*(i-1)+1 étoiles
    jusqu'à atteindre la Ne ligne qui aura donc 2*N-1 étoiles

    Argument : entier
    Valeur de retour : aucune
    """
    print("Triangle isocèle de ",N," étoiles de côté")
    for ligne in range(1,N+1) :
        for colonneBlancs in range(N-ligne) :
            print(" ",end="")
        for colonneEtoiles in range(2*ligne-1) :
            print("*",end="")
        print() #passer à la ligne
    print()

def carrePlein(N):
    """Affichage d'un carré plein de N étoiles de côté
    Donc N lignes de N étoiles

    Argument : entier
    Valeur de retour : aucune
    """
    print("Carré plein de ",N," étoiles de côté")
    for ligne in range(N) :
        print(N*"*")
    print()

def carreCreux(N):
    """Affichage d'un carré creux de N étoiles de côté
    Donc 1ère et dernière ligne pleind 'étoiles
    puis N-2 lignes intermédiaires de 1 étoile + (N-2) blancs + 1 étoile 

    Argument : entier
    Valeur de retour : aucune
    """
    print("Carré creux de ",N," étoiles de côté")
    for colonne in range(N) : #1ère ligne
        print("*",end="")
    print() #passer à la ligne
    for ligne in range(N-2) : #lignes intermédiaires
        print("*",end="")
        for blancs in range(N-2) :
            print(" ",end="")
        print("*") #on passe à la ligne
    for colonne in range(N) : #dernière ligne
        print("*",end="")
    print()

"""Saisie de N, nombre de lignes ou colonnes ou ..."""
N = int(input("N ? "))
etoiles(N)
triangle(N)
carrePlein(N)
carreCreux(N)
