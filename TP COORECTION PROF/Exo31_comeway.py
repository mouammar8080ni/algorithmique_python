"""Programme calculant les termes de la suite de Conway"""

"""Je propose de dÃ©finir d'abord une fonction qui va me permettre de calculer Ã  partir d'une chaÃ®ne de caractÃ¨res (liste de caractÃ¨res) de calculer la chaÃ®ne de caratÃ¨res qui permet de composer le terme suivant de Conway
"""
def chaineSuivanteConway(L) :
    """Fonction renvoyant la chaÃ®ne constituant le terme suivant de Conway Ã  parti de L
    ParamÃ¨tre attendu : L, chaÃ®ne de caractÃ¨res (liste de caractÃ¨res)
    Valeur de retour : chaÃ®ne de caractÃ¨res
    """
    """On va construire une chaÃ®ne de caractÃ¨res : 
       . on commence par la chaÃ®ne vide
       . puis on rajoutera les termes en concatÃ©nant newL avec des entiers +str(nombre)"""
    newL = ""
    """On prend le premier caractÃ¨re de L et on compte tous les caractÃ¨res suivants s'ils sont Ã©gaux"""
    init = 0 #caractÃ¨re de rÃ©fÃ©rence : le premier de valeur diffÃ©rente au prÃ©cÃ©dent, au dÃ©but on regarde le premier caractÃ¨re L[0]
    #tant qu'on n'a pas vu tous les caractÃ¨res de L on continue
    while init < len(L) :
        terme = init+1 #emplacement du premier caractÃ¨re Ã  comparer avec L[init]
        nbCaract = 1 #nb de caractÃ¨res semblables Ã  L[init] y compris lui-mÃªme
        while terme < len(L) and L[terme] == L[init] : #tant que les caractÃ¨res qui suivent L[init] sont les mÃªmes et qu'on n'est pas Ã  la fin de la liste
            nbCaract = nbCaract + 1 #on incrÃ©mente le nombre de caractÃ¨res semblables
            terme = terme+1 #on va voir le caractÃ¨re suivant
        #quand on sort de ce while, on a trouvÃ© le nb de caractÃ¨res semblables Ã  L[init]
        #on ajoute Ã  la liste newL le nombre de L[init] suivi de la valeur L[init]
        newL = newL+str(nbCaract)+str(L[init])
        init = terme #on est sorti du "while L[terme] == L[init]..." car L[init]!=L[terme] donc on repart de L[terme], nouveau terme Ã  regarder sauf si terme>=len(L) auquel cas on sortira aussi du "while init < len(L)"
    return newL #on a vu tous les Ã©lÃ©ments de L, on a fini

def calculerConway(n) :
    """Fonction retournant la chaÃ®ne correspondant au niÃ¨me terme de la suite de Conway

    ParamÃ¨tre attendu : n, entier
    Valeur de retour : chaÃ®ne de caractÃ¨res
    """
    L="1" #1Ã¨re chaÃ®ne de caractÃ¨res quand n==1
    for terme in range(2, n+1) : #on commence Ã  n==2 pour calculer les listes l'une aprÃ¨s l'autre
        L = chaineSuivanteConway(L) 
    
    return L


"""Programme principal"""
"""Saisie du numÃ©ro du terme dont on veut trouver la chaÃ®ne de caractÃ¨res renvoyÃ©e par la suite de Conway"""
nombre = int(input("Entrez le terme pour la suite de Conway : "))

"""Affichage de la chaÃ®ne de caractÃ¨res"""
print("Conway("+str(nombre)+") = "+calculerConway(nombre))
