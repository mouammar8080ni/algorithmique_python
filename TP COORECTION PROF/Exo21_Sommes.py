#Exercice 1.
def somme1():
        """Saisie d'entiers et calcul de leur somme jusqu'à ce que 
        l'utilisateur saisisse 0 : le test d'arrêt se fait sur la valeur du nombre saisi
        Pour calculer la somme de plusieurs valeurs saisies, on va ajouter à la somme
        chaque valeur dès qu'elle est saisie
        Argument : aucun
        Valeur de retour : entier, somme
        """
        somme = 0 #on commence par mettre 0 dans la somme (pas d'élément saisi encore)
        nombre = int(input("Nombre ? "))  #on saisit un 1er nombre 

        while nombre!=0 : #on teste si le nombre est non nul avant de le rajouter à la somme, s'il est nul on s'arrête
                somme = somme + nombre
                nombre = int(input("Nombre ? ")) #on saisit un nouveau nombre
        return somme

#Exercice 2
def somme2():
        """Saisie d'entiers et calcul de leur somme jusqu'à ce que 
        la somme soit supérieure à 40, le test d'arrêt est donc sur la valeur de la somme
        Pour le calcul de la somme c'est la même démarche que précédemment
        """
        somme = 0 #on commence par mettre 0 dans la somme (pas d'élément saisi encore)
        #on n'a plus besoin de saisir une valeur avant de commencer la boucle, puisque
        #là c'est la valuer de la somme qu'on veut tester, on s'arrête quand somme>40 

        while somme <= 40 : #on teste si la somme est atteinte avant de rajouter une valeur
                nombre = int(input("Nombre ? ")) #on saisit un nombre,
                somme = somme + nombre  #puis on l'ajoute à la somme
    	        #si vous échangez ces 2 instructions alors vous saisirez un nombre qui ne sera pas pris en compte dans votre somme !
        return somme

"""Programme principal"""
print("La saisie s'arrête quand un nombre saisi vaut 0 !")
print("Somme = ",somme1())
print("La saisie s'arrête quand la somme des nombres saisis dépasse 40 !")
print("Somme = ",somme2())


