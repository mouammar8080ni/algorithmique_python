"""Programme calculant la somme de 2 nombres entiers"""

def calculerSomme(x,y) :
    """Fonction rÃ©cursive renvoyant x+y, avec x et y entiers quelconques (positif ou nÃ©gatif :
    . test d'arrÃªt : si x==0, alors x+y==y
    . formules de rÃ©cursivitÃ© : 
            . si x>0 alors x+y = (x-1) + (y+1), on dÃ©crÃ©mente x pour arriver Ã  0
            . si x<0 alors  x+y = -(-x-y) = - ((-x) + (-y)), et on peut appliquer la formule prÃ©cÃ©dente en renvoyant - calculerSomme(-x, -y), du coup -x est positif et c'est ok !

    ParamÃ¨tres attendus : x et y entiers
    Valeur de retour : entier
    """

    """test d'arrÃªt"""
    if x==0 : 
        return y
    
    """rÃ©cursivitÃ©"""
    if x>0 :
        return calculerSomme(x-1,y+1)
    #quand on arrive ici x!=0 et x<=0, donc x<0
    return -calculerSomme(-x, -y) #on applique la formule de rÃ©cursivitÃ©

"""Programme principal"""
"""Saisie des nombres x et y"""
x = int(input("Entrez un entier : "))
y = int(input("Entrez un entier : "))

"""Affichage de x puissance y"""
print("("+str(x)+")+("+str(y)+") = ",calculerSomme(x,y))
