def croissant(a,b):
    """On va vérifier que a<b, sinon on les échangera

    Arguments : 2 entiers
    Valeurs de retour : 2 entiers
    """
    if a > b :
        return b, a
    return a,b

def somme_AB(a,b):
    """Somme des entiers compris entre a et b
    si a=4 et b=10, on veut donc faire la somme de 4+5+6+7+8+9+10
    on va ajouter ces nombres 1 par 1 à la somme, soit a, puis a+1, puis a+2, .. puis b

    Arguments : 2 entiers
    Valeur de retour : entier, somme
    """

    a,b = croissant(a,b)
    """On va ajouter à la somme a, a+1, ... b inclus => range(a,b+1)"""   
    somme= 0 #initialiser la somme à 0, on aurait pu l'initialiser à a et faire une boucle range(a+1,b+1)
    for val in range(a, b+1) : 
        somme = somme+val
    return somme


    
"""Programme principal"""
"""On va saisir la valeur de a et la valeur de b"""
a = int(input("a ? ")) 
b = int(input("b ? "))
print("La somme des entiers de "+str(a)+" à "+str(b)+" est ",somme_AB(a,b))

