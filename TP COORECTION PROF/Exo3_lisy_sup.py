"""Recherche de l'indice d'un Ã©lÃ©ment dans une liste"""
from random import * #pour les valeurs alÃ©atoires de la liste

def initList(nbLignes, nbColonnes) :
    """Fonction retournant une liste de 'nbLignes' listes de 'nbColonnes' entiers remplie par saisie des valeurs par l'utilisateur
    
    ParamÃ¨tres attendus : nbLignes, nbColonnes : entiers
    Valeur de retour : liste
    """
    L = [] 
    for ligne in range(nbLignes) :
        sousL = []
        for colonne in range(nbColonnes):
            val = int(input("L["+str(ligne)+","+str(colonne)+"] = "))
            sousL.append(val)
        L.append(sousL)
        
    return L

def afficheList(L) :
    """Fonction affichant une liste de listes d'entiers (matrice) ligne par ligne
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Pas de valeur de retour :
    """
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste
    for ligne in range(nbLignes) :
        for colonne in range(nbColonnes):
            print(L[ligne][colonne],end=" ")
        print()

            
def rechercheValExtremite(L) :
    """Fonction permettant de retourner la plus petite valeur et la plus grande valeur de la liste L passÃ©e en paramÃ¨tre
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Valeurs de retour : plus petite, plus grande (entiers)
    """
    plusPetite = L[0][0] #plus petite valeur
    plusGrande = L[0][0] #plus grande valeur
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste))    
    for ligne in range(nbLignes) :
        for colonne in range(nbColonnes):
            if plusPetite > L[ligne][colonne] :
                plusPetite = L[ligne][colonne]
            else : #n'est pas obligatoire, mais si on a trouvÃ© que L[ligne][colonne] est plus petit que la plus petite valeur alors il ne pourra pas Ãªtre plus grand que la plus grande ... Ã§a Ã©vite de tester la ligne suivante => gain de temps
                if plusGrande < L[ligne][colonne] :
                    plusGrande = L[ligne][colonne]
    return plusPetite, plusGrande #attention Ã  l'ordre de renvoi
             
def moyenne(L) :
    """Fonction permettant de retourner la moyenne des Ã©lÃ©ments de la liste L passÃ©e en paramÃ¨tre
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Valeur de retour : somme, entier
    """
    somme = 0 #contient la somme des valeurs
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste))    
    for ligne in range(nbLignes) :
        for colonne in range(nbColonnes):
            somme = somme + L[ligne][colonne]

    return somme/(nbLignes*nbColonnes)

def sommeEntiersPositifs(L) :
    """Fonction permettant de retourner la somme des entiers positifs de la liste L passÃ©e en paramÃ¨tre
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Valeur de retour : somme, entier
    """
    somme = 0 #contient la somme des valeurs
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste))    
    for ligne in range(nbLignes) :
        for colonne in range(nbColonnes):
            if L[ligne][colonne] > 0 :
                somme = somme + L[ligne][colonne]

    return somme 
             
"""Programme principal"""
"""Saisie de la liste de dÃ©part, on demande le nombre d'Ã©lÃ©ments"""
M = int(input("Entrez le nombre de lignes de la matrice : "))
N = int(input("Entrez le nombre de colonnes de la matrice : "))
matrice = initList(M,N)
print(matrice) #affiche la liste L sous forme de liste
afficheList(matrice) #affiche la liste sous forme de matrice
PP, PG = rechercheValExtremite(matrice)
print("Les valeurs de la liste appartiennent Ã  l'intervalle ["+str(PP)+","+str(PG)+"]")
print("La moyenne des valeurs de la liste est Ã©gale Ã  : "+str(moyenne(matrice)))
print("La somme des valeurs positive de la liste est Ã©gale Ã  : "+str(sommeEntiersPositifs(matrice)))
