"""Saisie de l'année"""
a = int(input("Année ? "))

"""a est bissextile si elle est (multiple de 4 et pas de 100) ou (multiple de 400)"""

"""a n'est pas bissextile si elle est (non multiple de 4 ou multiple de 100) et (non multiple de 400) 
<=> (non multiple de 4 et non multiple de 400) OU (multiple de 100 et non multiple de 400)
<=>(non multiple de 4) OU (multiple de 100 et non multiple de 400)
"""

if a%4 or (not(a%100) and a%400) : #signifie que a%4!=0 ou (a%100==0 et a%400!=0)
    print(a," n'est pas bissextile")
else :  #voir explication ci-dessus
    print(a," est bissextile")
       
