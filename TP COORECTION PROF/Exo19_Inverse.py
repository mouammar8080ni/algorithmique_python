def inverse(n):
        """Fonction qui demande laisie d'un réel tant que sa valeur est égale à 0
	
        Argument : aucun
        Valeur de retour : entier différent de 0
        """
        return 1/n

def saisie():
        """Fonction qui demande laisie d'un réel tant que sa valeur est égale à 0
	
        Argument : aucun
        Valeur de retour : entier différent de 0
        """
        nombre = 0 #permet de rentrer dans la boucle au moins une fois pour que l'utilisateur choisisse sa valeur
        while nombre==0 :
                nombre=int(input("Nombre ? "))
        return nombre

"""Programme principal : affiche la valeur de l'inverse d'un nombre"""
nombre = saisie()
print("1/"+str(nombre)+" = "+str(inverse(nombre)))
