"""Entrée des 2 premiers termes"""
u0 = int(input("u0 ? "))
u1 = int(input("u1 ? "))

"""Calcul des 4 termes suivants"""
"""6 variables => une variable pour chaque terme"""
u2 = u1 + u0
u3 = u2 + u1
u4 = u3 + u2
u5 = u4 + u3
"""On affiche les 6 termes"""
print("Les termes de la suite sont : ",u0, " ",u1, " ",u2, " " ,u3, " ",u4, " ",u5)
print()

"""3 variables => pour écrire u2 = u1 + u0,
et pour chaque terme suivant (u3 = u2 + u1) on voit que
u1 va avoir comme nouvelle valeur u2 et u0 aura u1
"""
print("Les termes de la suite sont : ",u0, " ",u1, end="   ") #on affiche tout de suite les termes avant de modifier leur valeur
u2 = u1 + u0 #3e terme
print(u2, end="   ")
u0 = u1 #attention on n'a plus besoin de u0 alors qu'on doit sauvegarder u1 avant de l'écraser
u1 = u2
u2 = u1 + u0 #4e terme
print(u2, end="   ")
u0 = u1 
u1 = u2
u2 = u1 + u0 #5e terme
print(u2, end="   ")
u0 = u1 
u1 = u2
u2 = u1 + u0 #6e terme
print(u2)

"""2 variables => pour écrire u0 = u1 + u0,
quand on calcule u2 : u1 + u0, on n'a plus besoin de la valeur de u0 pour calculer ensuite u3, donc on peut sauvegarder la valeur de u1+u0 dans la variable "u0"=> u0 = u1+u0 (on a la valeur du 3e terme u2 dans la variable "u0")
Puis pour calculer la valeur du 4e terme (u3), on a dans la variable "u0" la valeur de u2 (3e terme), dans la variable "u1" la valeur de u1 (2e terme) dont on n'aura plus besoin pour calculer le 5e terme. Donc on va sauvegarder la valeur du 4e terme (u3) dans la variable "u1" => u1 = u0+u1 (on a la valeur du 4e terme u3 dans la variable "u1")
Puis pour calculer la valeur du 5e terme (u4), on a dans la variable "u1" la valeur de u3 (4e terme), dans la variable "u0" la valeur de u2 (3e terme) dont on n'aura plus besoin pour calculer le 6e terme. Donc on va sauvegarder la valeur du 5e terme (u4) dans la variable "u0" => u0 = u1+u0 (on a la valeur du 5e terme u4 dans la variable "u0")

"""
"""Entrée des 2 premiers termes"""
u0 = int(input("u0 ? "))
u1 = int(input("u1 ? "))
print("Les termes de la suite sont : ",u0, " ",u1, end="   ")
u0 = u1 + u0 #3e terme u2
print(u0, end="   ")
u1 = u1 + u0 #4e terme u3
print(u1, end="   ")
u0 = u1 + u0 #5e terme u4
print(u0, end="   ")
u1 = u1 + u0 #6e terme u5
print(u1)
