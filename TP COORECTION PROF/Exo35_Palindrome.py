"""Palindrome"""


def palindrome(L) :
    """Fonction renvoyant 1 si la chaÃ®ne de caractÃ¨res L est un palindrome
    et 0 sinon
    ParamÃ¨tre attendu : L, liste de caractÃ¨res
    Valeur de retour : 1 si L est un palindrome, 0 sinon
    """
    """positions des caractÃ¨res Ã  comparer"""
    first = 0       #caractÃ¨re dÃ©but de chaÃ®ne
    last = len(L)-1 #caractÃ¨re fin de chaÃ®ne
    """variable permettant de s'arrÃªter dÃ¨s que 2 caractÃ¨res comparÃ©s sont diffÃ©rents"""
    encore = 1      #on continue

    while first < last and encore : #on compare tous les caractÃ¨res de la liste
        """On supprime les caractÃ¨res blancs :
        . si on regarde L[first] alors on veut regarder le caractÃ¨re suivant L[first+1]
        . si on regarde L[last] alors on veut regarder le caractÃ¨re prÃ©cÃ©dent L[last-1]
        """
        while first < last and L[first]==" " : #on ne compte pas les espaces
            first = first+1
        while first < last and L[last]==" " :  #on ne compte pas les espaces
            last = last - 1
        if L[first] == L[last] : #si les 2 caractÃ¨res sont les mÃªmes, on regarde les 2 suivants
            first = first + 1
            last = last - 1
        else : # on s'arrÃªte
                encore = 0
    return encore


"""Programme principal : on teste si une chaÃ®ne de caractÃ¨res saisie est un palindrome"""
L = input("Entrez une chaÃ®ne de caractÃ¨res (mot ou phrase avec des mots sÃ©parÃ©s par des espaces) : ")

if palindrome(L) :
    print("'"+L+"' est un palindrome")
else :
    print("'"+L+"' n'est pas un palindrome")
    
