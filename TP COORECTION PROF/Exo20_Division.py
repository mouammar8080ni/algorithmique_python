def saisie() :
    """Saisie de 2 réels un dividende et un diviseur 
    argument : aucun
    valeur de retour : 2 réels
    """
    dividende = float(input("Dividende ? "))
    diviseur = float(input("Diviseur ? "))
    return dividende, diviseur

def division(dd, ds):
    """Calcule la divisio de dd par ds
    argument : 2 réels
    valeur de retour : aucune
    """
    print(str(dd)+"/"+str(ds)+" = "+str(dd/ds))


def saisieDiviseurNonNul() :
    """Saisie de 2 réels un dividende et un diviseur dont le dernier doit être non nul
    argument : aucun
    valeur de retour : 2 réels
    """
    dividende = float(input("Dividende ? "))
    diviseur = float(input("Diviseur ? ")) #on choisit d'abord une valeur pour pouvoir écrire un message d'erreur si la saisie est mauvaise!
    while diviseur==0 :
        print("Erreur !")
        diviseur=float(input("Diviseur ? "))
    return dividende, diviseur

"""Calcul et affichage de la division de ces 2 nombres"""
dd, ds = saisie()
division(dd, ds)
dd, ds = saisieDiviseurNonNul()
division(dd, ds)
