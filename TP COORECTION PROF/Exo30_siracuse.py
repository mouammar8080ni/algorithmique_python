def estPair(n):
    """renvoie 1 si n est pair et 0 sinon

    argument attendu : n, entier
    valeur de retour : 0 si n est impair et 1 sinon
    """
    return not(n%2) #si n%2==0 c'est que n est pair on retourne 1, sinon on retourne 0

def calculerSyracuse(un):
    """renvoie le terme suivant de un dans la suite de Syracuse

    argument attendu : un, terme entier
    valeur de retour : terme suivant "un", entier
    """
    if estPair(un) : #appel de la fonction prÃ©cÃ©dente
        return int(un/2)
    else :
        return int(3*un + 1)

def suiteSyracuse(u0):
    """affiche les termes de la suite de Syracuse Ã  partir de u0
    jusqu'Ã  ce que la valeur obtenue soit 1

    argument attendu : u0, entier, terme initial de la suite
    valeur de retour : aucun
    """
    print(u0,end=" ")
    while u0!=1 :
        u0 = calculerSyracuse(u0)
        print(u0,end=" ")

        
def suiteSyracuse(u0):
    """affiche les termes de la suite de Syracuse Ã  partir de u0
    jusqu'Ã  ce que la valeur obtenue soit 1

    argument attendu : u0, entier, terme initial de la suite
    valeur de retour : aucun
    """
    print(u0,end=" ")
    while u0!=1 :
        u0 = calculerSyracuse(u0)
        print(u0,end=" ")

def suiteSyracuse_Ntermes(u0,N):
    """affiche les N termes de la suite de Syracuse Ã  partir de u0

    arguments attendus : u0 : entier, N : nb de termes Ã  calculer
    valeur de retour : aucun
    """
    print(u0,end=" ")
    for i in range(N-1) :
        u0 = calculerSyracuse(u0)
        print(u0,end=" ")


"""Programme principal"""
u0 = int(input("Entrez un entier positif non nul : "))
while u0 <=0 :
    u0 = int(input("Erreur, recommencez la saisie : "))
print("La suite de Syracuse est :")
suiteSyracuse(u0)
print()
"""Appel de la 2e fonction"""
u0 = int(input("Entrez un entier positif non nul : "))
while u0 <=0 :
    u0 = int(input("Erreur, recommencez la saisie : "))
print("La suite de Syracuse est :")
N = int(input("Entrez le nombre de termes voulus : "))
print("Les ",N," termes de la suite de Syracuse Ã  partir de ",u0," sont :")
suiteSyracuse_Ntermes(u0,N)
print()
