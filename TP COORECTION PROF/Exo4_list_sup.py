"""Recherche de l'indice d'un Ã©lÃ©ment dans une liste"""
from random import * #pour les valeurs alÃ©atoires de la liste

def initList(nbLignes, nbColonnes) :
    """Fonction retournant une liste de 'nbLignes' listes de 'nbColonnes' entiers remplie par saisie des valeurs par l'utilisateur
    
    ParamÃ¨tres attendus : nbLignes, nbColonnes : entiers
    Valeur de retour : liste
    """
    L = [] 
    for ligne in range(nbLignes) :
        sousL = []
        for colonne in range(nbColonnes):
            sousL.append(-2+randrange(nbLignes*nbColonnes))
        L.append(sousL)
        
    return L

def afficheList(L) :
    """Fonction affichant une liste de listes d'entiers (matrice) ligne par ligne
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Pas de valeur de retour :
    """
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste
    for ligne in range(nbLignes) :
        for colonne in range(nbColonnes):
            print(L[ligne][colonne],end=" ")
        print()

def transposeeMatrice(L) :
    """Fonction permettant de retourner la transposÃ©e de la liste L (matrice) passÃ©e en paramÃ¨tre
    
    ParamÃ¨tre attendu : L : liste (= matrice)
    Valeur de retour : liste (=matrice)
    """
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste))
    matTransposee = []
    """les sous-listes contiennent maintenant les Ã©lÃ©ments de chaque colonne, donc pour une colonne, on affiche dans une sous-liste les Ã©lÃ©ments de chaque ligne
    """
    for colonne in range(nbColonnes): #pour chaque colonne
        sousL = []
        for ligne in range(nbLignes) : #on met les Ã©lÃ©ments des lignes
            sousL.append(L[ligne][colonne])
        matTransposee.append(sousL)
        
    return matTransposee
            
             
"""Programme principal"""
"""Saisie de la liste de dÃ©part, on demande le nombre d'Ã©lÃ©ments"""
M = int(input("Entrez le nombre de lignes de la matrice : "))
N = int(input("Entrez le nombre de colonnes de la matrice : "))
matrice = initList(M,N)
print(matrice) #affiche la liste L sous forme de liste
matT = transposeeMatrice(matrice)
afficheList(matrice) #affiche la liste sous forme de matrice
print("a pour transposÃ©e :")
afficheList(matT) #affiche la liste sous forme de matrice
