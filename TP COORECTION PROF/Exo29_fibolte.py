"""Programme calculant le nieme terme de la suite de Fibonacci"""

def calculerFibonacci(n) :
    """Fonction renvoyant le niÃ¨me terme de la suite de Fibonacci

    ParamÃ¨tre attendu : n, entier
    Valeur de retour : entier, niÃ¨me terme 
    """
    #On connaÃ®t les valeurs de la suite pour n==1 et n==2
    if n==1 or n==2 :
        return 1
    """Sinon il faut utiliser le fait que u3 = u2 + u1 et que la fois suivante :
    u1 prendra la valeur de u2, et u2 prendra la valeur de u3, on pourra ainsi
    calculer le 4Ã¨me terme de Fibonacci en rÃ©-exÃ©cutant u2 + u1
    """
    valNMoins2 = 1 #valeur de dÃ©part de u1
    valNMoins1 = 1 #valeur de dÃ©part de u2
    for terme in range(3, n+1) : #le premier terme calculÃ© correspond au 3Ã¨me terme de Fibonacci et on veut calculer le niÃ¨me terme : ici "terme" prend ses valeurs dans [3, 4, .. n]
        valN = valNMoins1 + valNMoins2
        valNMoins2 = valNMoins1
        valNMoins1 = valN
    #Quand on sort du "for", on a dans valN la valeur du niÃ¨me terme de Fibonacci
    return valN


"""Programme principal"""
"""Saisie du terme de la suite qu'on veut calculer"""
Nterme = int(input("Entrez le numÃ©ro du terme de Fibonacci dÃ©sirÃ© : "))

"""Affichage de fibo(Nterme)"""
print("fibo("+str(Nterme)+") = ",calculerFibonacci(Nterme))
