"""Programme donnant la valeur absolue d'un nombre"""

def calculerAbsolu(x) :
    """Fonction renvoyant la valeur absolue de x

    Paramètre attendu : x, réel
    Valeur de retour : réel, valeur absolue de x
    """
    if x >=0 :
        return x
    else :
        return -x

"""On peut aussi écrire la même fonction en supprimant le "else"
En effet :
. si x est positif ou nul alors on va exécuter "return x", on sort de la fonction
. si x est négatif alors on n'exécutera pas "return x", donc on passe à l'instruction suivante
"""
def calculerAbsolu(x) :
    """Fonction renvoyant la valeur absolue de x

    Paramètre attendu : x, réel
    Valeur de retour : réel, valeur absolue de x
    """
    if x >=0 :
        return x
    return -x


"""Programme principal"""
"""Saisie d'un nombre"""
nombre = float(input("Entrez un nombre : "))

"""Affichage de sa valeur absolue"""
"""la fonction "calculerAbsolu renvoie un nombre qu'on peut afficher directement
On pourrait aussi suavegarder le résultat dans une variable et afficher le contenu de cette variable ensuite (mais c'est inutile si on n'a pas besoin de réutiliser la valeur absolue dans un calcul :
nbAbs = calculerAbsolu(nombre)
print("abs("+str(nombre)+") = ",nbAbs)
"""
print("abs("+str(nombre)+") = ",calculerAbsolu(nombre))
