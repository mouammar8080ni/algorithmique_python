def saisieNom():
    """Saisie d'une chaîne de caractères et affichage
    
    Argument : aucun
    Valeur de retour : aucune
    """

    """Saisie du nom"""
    nom = input("Nom ? ") #pas besoin de caster, on attend une chaîne de caractères

    """On affiche "Bonjour " le nom entré 
    sauf si la chaîne de caractères est vide, auquel cas on affiche "Au revoir"
    """
    while nom!="" :
        print("Bonjour "+nom+"!")
        nom = input("Nom ? ") #si nom n'est pas saisi (touche "entrée") alors on arrête
    print("Au revoir!") #on est sorti de la boucle

"""Programme principal"""
saisieNom()
