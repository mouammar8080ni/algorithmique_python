"""Recherche de l'indice d'un Ã©lÃ©ment dans une liste"""
from random import * #pour les valeurs alÃ©atoires de la liste

def initListParite(nbLignes, nbColonnes) :
    """Fonction retournant une liste de 'nbLignes' listes de 'nbColonnes' entiers remplie avec -1 si les cases L[i][j] sont paires (i+j pair) et 1 si cases impaires (i+j impair)
    
    ParamÃ¨tre attendu : n : entier (taille de la matrice carrÃ©e)
    Valeur de retour : liste
    """
    L = [] 
    for ligne in range(nbLignes) :
        sousL = []
        for colonne in range(nbColonnes):
            if (ligne+colonne)%2==0 :
                sousL.append(-1)
            else :
                sousL.append(1)
        L.append(sousL)
        
    return L

def afficheList(L) :
    """Fonction affichant une liste de listes d'entiers (matrice) ligne par ligne
    
    ParamÃ¨tre attendu : L : liste (matrice)
    Pas de valeur de retour :
    """
    nbLignes=len(L) #nombre de lignes (=sous-listes de L)
    nbColonnes=len(L[0]) #nombre de colonnes (=nb d'Ã©lÃ©ments de chaque sous-liste
    for ligne in range(nbLignes) :
        for colonne in range(nbColonnes):
            print(L[ligne][colonne],end=" ")
        print()
        
            
"""Programme principal"""
"""Saisie de la liste de dÃ©part, on demande le nombre d'Ã©lÃ©ments"""
M = int(input("Entrez le nombre de lignes de la matrice : "))
N = int(input("Entrez le nombre de colonnes de la matrice : "))
matrice = initListParite(M,N)
print(matrice) #affiche la liste L sous forme de liste
afficheList(matrice) #affiche la liste sous forme de matrice
