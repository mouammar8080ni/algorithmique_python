"""Saisie du jour"""
jour = int(input("Jour ? "))
"""Saisie du mois"""
mois = int(input("Mois ? "))
"""Saisie de l'année"""
annee = int(input("Année ? "))

"""une date est valide si :
. mois == 1, 3, 5, 7, 8, 10, 12 et 1<=jour<=31
. OU mois == 4, 6, 9, 11 et 1 <= jour <=30
. OU mois == 2 et (1<=jour<=28 OU (jour==29 et année bissextile))
avec annee est bissextile si elle est (multiple de 4 et pas de 100) ou (multiple de 400)"""

"""on regarde les mois dont les jours sont en 31"""
if mois==1 or mois==3 or mois==5 or mois==7 or mois==8 or mois ==10 or mois==12 :
    if 1 <= jour and jour <= 31 :
        print("Date Valide !")
    else : #jour<1 ou jour>31
        print("Date Non Valide")
else : #tous les mois autres que 1,3,5,7,8,10,12
    if mois==4 or mois==6 or mois==9 or mois==11 :
        if 1 <= jour and jour <= 30 :
            print("Date Valide !")
        else : #mois = 4,6,9,11 ET (jour<1 ou jour >30) mois valide mais pas le jour
            print("Date Non Valide")
    else : #tous les mois autres que 1,3,5,7,8,10,12, + 4,6,9,11
        if mois==2 : #on teste pour mois==2, dernier non teste
            if 1 <= jour and jour <= 28 :
                print("Date Valide !")
            else : #mois==2 et jour<1 ou jour>28)
                if jour==29 and ((not(annee%4) and annee%100) or not(annee%400)) : #jour==29 et annee bissextile
                    print("Date Valide !")
                else : #mois==2 et (j<1 et j>29), mois valide mais pas le jour
                    print("Date Non Valide !")
        else : #tous les mois autres que 1,3,5,7,8,10,12, + 4,6,9,11 + 2 => mois invalide
            print("Date Non Valide !")
            
 
"""ON peut aussi faire en une seule fois en inscrivant toutes les conditions 
pour qu'une date soit valide. Mais ce n'est pas très lisible !
"""

if ((mois==1 or mois==3 or mois==5 or mois==7 or mois==8 or mois ==10 or mois==12) and (1 <= jour and jour <= 31)) \
    or ((mois==4 or mois==6 or mois==9 or mois==11) and (1 <= jour and jour <= 30)) \
    or (mois==2 and ((1<=jour and jour<=28) or (jour==29 and ((not(annee%4) and annee%100) or not(annee%400))))) : 
    print("Date Valide !")
else :  
    print("Date Non Valide !")
