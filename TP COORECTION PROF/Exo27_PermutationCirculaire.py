def createList():
    """Création de la liste avec des valeurs de 0 à 49
    Argument : aucun
    Valeur de retour : liste
    """
    L = []
    for elt in range(50) : #elt va prendre les valeurs 0, puis 1, puis 2, ..., 49
        L.append(elt)
    return L


def afficheList(L):
    """On affiche la liste élément par élément
    Argument : liste
    Valeur de retour : aucune
    """
    print("Liste : ")
    for indice in range(50) :
        print(L[indice],end=" ")
    print() #pour finir en passant à la ligne
    #pour afficher une liste on peut aussi écrire directement
    #print("Liste initiale bis : ")
    #print(L)

def permuteList(L):
    """Permutation circulaire à gauche
    On veut que :
    . le 2e élément (L[1]) se retrouve à la place du 1er (L[0]) => L[0] = L[1]
    . le 3e élément (L[2]) se retrouve à la place du 2e (L[1]) => L[1] = L[2]
    . le 4e élément (L[3]) se retrouve à la place du 3e (L[2]) => L[2] = L[3]
    ...
    . le 50e élément (L[49]) se retrouve à la place du 49e (L[48]) => L[48] = L[49]
      => L[i] = L[i+1] avec i qui va de 0 à 48 inclus
    . puis on veut que dans le 1er élément se retrouve à la place du 50e  
      => L[49] = L[0] sauf que la valeur initiale de L[0] a été écrasée par la 1ère ligne  L[0] = L[1]
    Il faut donc avant d'appeler la boucle sur L[i] = L[i+1], il faut sauvegarder la valeur de L[0], et c'est cette valeur qu'on mettra dans L[49]

    Argument : liste
    Valeur de retour : liste
    """
    save = L[0] #on sauvegarde la valeur de L[0] dans la variable "save"
    for indice in range(49) : #indice prend des valeurs de 0 à 48
        L[indice] = L[indice+1]
    L[49] = save #on met dans le dernier élément la valeur de L[0] initiale
    print() #pour finir en passant à la ligne
    return L

"""Programme principal"""
L = createList()
afficheList(L)
print("On permute la liste")
L = permuteList(L)
afficheList(L)
