def saisirEntierImpair():
    nbr = int(input("Entrer une valeur entre 3 et 19: "))
    if nbr>19 or nbr<3:
        return saisirEntierImpair()
    if nbr%2 ==0:
        return saisirEntierImpair()
    else:
        return nbr 

def initialiserCarre(n):
    l=list()
    for x in range(n):
        t=list()
        for i in range(n):
            t.append(0)
        l.append(t)
    return l

    
def rechercherProchainePosition(lig,col,cm,n):
    
    if lig !=0:
        nvlig=lig-1
    else:
        nvlig = n-1
    if col == n-1:
        nvcol = 0
    else: 
        nvcol=col +1
    
    if cm[nvlig][nvcol]==0:
        return nvlig, nvcol
    else:
        return lig+1, col

def  construireCarre(n):
    cm=initialiserCarre(n)
    lig=0
    col=(n//2)
    for x in range(n*n):
        cm[lig][col]=x+1
        lig,col=rechercherProchainePosition(lig,col,cm,n)
    return cm
def afficherCarre(cm,n):
    print("Carre magique d'ordre ",n)
    for x in range(n):
        for j in range(n):
            print(cm[x][j], end=" ")
        print()

def somLig(cm,n,t):
    som=0
    for x in range(len(cm[t])):
        som=som+cm[0][x]
    return som
def somD(cm):
    som = 0
    for k in range(len(cm)):
        som=som+cm[k][k]
    return som

        
def verifierCarre(cm,n):
    for j in range(n):
        som=somLig(cm,n,j)
        if som==somLig(cm,n,j+1):
            return som
        else:
            return False
    
    




n=3
# lig = 0
# col = 0
# cm = initialiserCarre(n)
saisi=saisirEntierImpair()

# lig,col=rechercherProchainePosition(lig,col,cm,n)
# print(lig)
# print(col)

cm = construireCarre(n)
afficherCarre(cm,n)
print(verifierCarre(cm,n))
print(somD(cm))
