# creation dune fonction qui calcul le PGDC de deuxnombres

def calculPgcd(a,b):
    if b==0:
        return a
    else :
        return calculPgcd(b,a%b)
# Tracer de la fonction preceente pour 4 et 3
print("le PGCD de 4 et 3 est ")
calculPgcd(4,3)
print(calculPgcd(4,3))

print()

#Une fonction qui calcul le PPCM de deux nombre 

def calculPpcm(a,b):
    pgcd=calculPgcd(a,b)
    if pgcd==a:
        return b
    else :
        return (a*b)/pgcd



# PROGRAMME PRINCIPAL
    
# Declaration des variable d'entrer par l'utilisateur 
x=int(input("Entrer la valeur de x : "))
y=int(input("Entrer la valeur de y: "))

# Appel de la fonction recursive du pgcd

PGDC=calculPgcd(x,y)
print("le pgcd de deux nombre saisi est :")
print(PGDC)

print()

#appel de la fonction recursive du ppcm

PPCM=calculPpcm(x,y)
print("le PPCM de deux nombres saisi est : ")
print(PPCM)


