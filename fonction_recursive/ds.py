from random import*
def init(n):
    L = []
    for i in range(n):
        L.append(randint(1,50))
    return L

def Tri_Selection (L,n):
    for taille in range(n, 1, -1): #taille du tableau à parcourir
        max = 0 #la position du plus grand élément
        for j in range (1,taille): #recherche du plus grand élément
            if L[j] > L[max]:
                max = j
        if max != taille-1 : #si le plus grand élément n'est pas déjà à sa place
            L[max], L[taille-1] = echanger(L[max], L[taille-1])
L = init(20) #on utilise la fonction vue dans le paragraphe précédent
print("liste à trier : \n", L)
Tri_Selection(L,20)
print("liste croissante : \n", L)
