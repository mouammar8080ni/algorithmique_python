# une fonction RECURSIVE qui calcul la puissance d'un nombre 

def calculPuissance(x,y):
    if y>0:
        return x*calculPuissance(x,y-1)
    else :
        if y==0:
            return 1
# pour les valeur 4 et 3
calculPuissance(4,3)
print("la reponse de 4 puissance 3 est de :")
print(calculPuissance(4,3))


# PROGRAMME PRINCIPAL
 
a=int(input("entrer la valeur de a : "))
b=int(input("entrer la valeur de b: "))

# appel de la fonction recursive 

P=calculPuissance(a,b)
print("la reponses est :")
print("Puiss=",P)