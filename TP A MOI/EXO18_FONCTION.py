"""UN PROGRAMME SUR LES CALCULES DES FONCTION
SUR DES FONCTIONS"""

"""Creation de fonction pour calculer des fonction""" 
def f1(x):
    if (x%2==0):  # permet de calculer la fonction de seconde degré si <x pair>
        return 3*x**2+x+1
    else :
        return x+1 #permet de calculer la fonction du premier degré si <x impair>

"""creation de la fonction à degré 2 sans les membre <b> ni <c>"""

def f2(x):
    if x>0: # calculer la valeur s'il est positif
        return 3*x**2
    
    else :
        return -3*x**2 # calcul la valeur s'il est positif

""" Fonction qui regroupe f1 et f2 a deux variable"""

def g(x,y):
    g1=f1(x) #Appel de la fonction f1 avec parametre<x>
    
    g2=f2(y) #Appel de la fonction f2 avec parametre <y>
    
    g==g1+g2
    return g1+g2

"""Une tracé de calcul de valeur avec <x=2> et <y=-3>"""

g(2,-3) #Appel de la fonction
print("Une tracé de calcul de g(2,-3) avec les fonctions f1 et f2 est:")
print("g(2,-3)=",end=" ") # affichage de la reponses
print(g(2,-3))


print() # saut d'une ligne s'il est declaré vide

"""creation d'une fonction g2 avec parametre <x> et <y>
    sans appeler les fonction precedente"""
def g2(x,y):
    if (x%2==0):
        M=3*x**2+x+1
    else :
        M=x+1
    if (y>0):
        N=3*y**2
    else :
        N=-3*y**2
    S=M+N
    return S

print("//////////////////////////////////////////////////////////////////")

"""Une tracé de calcul de valeur avec <x=2> et <y=-3>"""
g2(2,-3)
print("Une tracé de calcul de g(2,-3) sans les fonctions f1 et f2 est:")
print("g2(2,-3)=",end=" ")
print(g2(2,-3))


"""PROGRAMME PRINCIPALE"""
#Demandons à l'utlisateur d'entrer ces deux valeurs
    
x=int(input("Entrer un nombre entier x= "))
y=int(input("Entrer un nombre entier y= "))

"""APPEL DES FONCTIONS"""

#Appel de la fonction f1
F1=f1(x) 
print("la reponse de f1(",x,")=",end=" ") # Affichage de la fonction
print(F1)

#Appel de la fonction f2
F2=f2(x) 
print("la reponse de f2(",x,")=",end=" ")
print(F2)

print()

#Appel de la fonction g
G1=g(x,y)
print("avec f1 et f2 de paramettre <x> et <y> Donc:")
print("la reponse de g(",x,",",y,")=",end=" ")
print(G1)

print()

#Appel de la fonction g2 sans l'introduction de f1 et f2
G2=g2(x,y)
print("sans f1 et f2 de paramettre <x> et <y> Donc:")
print("la reponse de g2(",x,",",y,")=",end=" ")
print(G2)

    
    
