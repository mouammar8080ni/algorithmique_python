
"""Division et diviseur non nul"""

"""Fonction a deux paramettre de deux
entier et qui donne le resultat"""


#Fonction de calcul de division de deux nombre
def division(a,b):

    return a/b # retorune et donne le quotient de deux nombre


"""Creation d'une fonction saisi,sans tenir compte de la valeur du diviseur"""    
def saisi():
   
    a=int(input("Entrer le Dividende: "))
    b=int(input("Entrer le diviseur: "))
    return a,b


"""Creation d'une fonction saisi,forçant l'utilisateur a entrer un diviseur valide"""
def saisiNul():
    x=int(input("Entrer le Dividende: "))
    y=int(input("Entrer le diviseur: "))
    while y==0:
        print("Erreur!")
        y=int(input("Entrer le diviseur: "))
    return x,y

"""Programme principale"""

print('Programme sans tenir compte du diviseur\n\t !!!!!!!!!!!!!!!!!!!!!!!!!')
#Appel de la fonction saisi simple
a,b=saisi()# il faut toujours stoquer des valeurs quand utilise 'return' c'est ce qui a causé le baigue

D=division(a,b) # Appel de la fonction de calcul de la division
print(a,"/",b,"=",end=" ")
print(D)

print()

print("Programme avec tenir compte du diviseur\n\t !!!!!!!!!!!!!!!!!!!!!!")
x,y=saisiNul()
DNN=division(x,y) # Appel de la fonction de calcul de la division
print(x,"/",y,"=",end=" ")
print(DNN)


