""" INVERSE D'UN NOMBRE"""


"""Creation d'une fonction qui retourne
l'inverse d'un nombre"""

def inverse(n):

        return 1/n  # retourne l'inverse d'un nombre saisi par son resultat

"""Fonction permettant à l'utilisateur d'entrer un reel
et en lui forçant à saisir un reel different de 'zero',"""

def saisi():
    nombre = 0 # permettant d'entrer une fois au moin dans la boucle
    
    while (nombre==0):  # tant que nombre==0, l'utilisateur va commencé l'operation

        nombre=float(input("Entrer une valeur "))
        
    return nombre # retourne le nombr different de zero


"""PROGRAMME PRINCIPALE"""

nombre=saisi() #Appel de la fonction saisi,

IN=inverse(nombre) # Appel de la fonction inverse du nombre

print("1/",nombre,"=",end=" ") #Affichage de l'inverse et du resultat
print(IN)








