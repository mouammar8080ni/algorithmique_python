# un programme qui saisit une matrice M
# composé de 3ligne et 4colones 

def matrice_M():
    T=[]
    T1=[]
    T2=[]
    T3=[]
# Remplissage du sous list L1=T1

    n=str(input("L[0][0]=")) # la fonction <str> permet de declarer des caracteres et des nombres
    T1.append(n)
    n=str(input("L[0][1]="))
    T1.append(n)
    n=str(input("L[0][2]="))
    T1.append(n)
    n=str(input("L[0][3]="))
    T1.append(n)
# Remplissage du sous list L2=T2
    n=str(input("L[2][0]="))
    T2.append(n)
    n=str(input("L[2][1]="))
    T2.append(n)
    n=str(input("L[2][2]="))
    T2.append(n)
    n=str(input("L[2][3]="))
    T2.append(n)

# Remplissage du sous list L3=T3
    n=str(input("L[3][0]="))
    T3.append(n)
    n=str(input("L[3][1]="))
    T3.append(n)
    n=str(input("L[3][2]="))
    T3.append(n)
    n=str(input("L[3][3]="))
    T3.append(n)

# Remplissage du grand list L=T
    T.append(T1)
    T.append(T2)
    T.append(T3)

    return T

#Creation d'une fonction qui affiche les sous liste en Matrice

def affichage_Matrice(T):
    for i in range(len(T)): # le nombre d'element de chaque ligne ou colone
        for j in range(4): # nombre de ligne,j parcour chaque element d'une ligne ou colone
            print(T[i][j],end=" ")
        print()

# creation d'une fonction qui permet d'afficher la matrice transposee

def affichage_transpose(T):
    for i in range(4):
        for j in range(3):
            print(T[j][i], end=" ")
        print()

# PROGRAMME PRINCIPAL

#Affichage Matrice Normal
T=matrice_M()
affichage_Matrice(T)

#Affichage Matrice transposée
print("la matrice transposee est :")
affichage_transpose(T)
